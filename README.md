# Devices GUI Web App (devices-gui)

## Project setup
```shell script
npm install
```

### Compiles and hot-reloads for development
```shell script
npm run serve
```

### Compiles and minifies for production
```shell script
npm run build
```

### Run your unit tests
```shell script
npm run test:unit
```

### Lints and fixes files
```shell script
npm run lint
```

### Dockerize
To create a Docker image of the application, including any/all runtime dependencies, run the following Docker command.
```shell script
docker build -t bartman111/device-gui:0.1.0 .
```

### Kubernetes Deploy
To deploy the application as a microservice, run the following Kubernetes `kubectl` command
```shell script
kubectl apply -f k8s.yaml
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
