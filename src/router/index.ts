import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import About from '../views/About.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'About',
    component: About,
  },
  {
    path: '/devicestatus',
    name: 'Device Status',
    component: () => import(/* webpackChunkName: "about" */ '../views/DeviceStatus.vue'),
  },
  {
    path: '/devicesaffected',
    name: 'Devices Affected',
    component: () => import(/* webpackChunkName: "about" */ '../views/DevicesAffected.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
